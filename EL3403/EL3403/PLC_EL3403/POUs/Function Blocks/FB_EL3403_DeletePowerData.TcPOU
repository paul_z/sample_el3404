﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.0.24">
  <POU Name="FB_EL3403_DeletePowerData" Id="{a98ef070-359c-4d63-8ecd-fd58325f6534}">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_EL3403_DeletePowerData
(***************************************************************************
	This function block has been written for Terminal Firmware version 9
	
	Written by:	Paul Zurlinden
				Beckhoff Automation
				*Updated from sample code provided by Kurt W.
	
	Date:		2014-07-17
***************************************************************************)
VAR_INPUT
	i_xExecute		: 	BOOL;
	i_eChannel 		:	ENUM_PowerChannel;
	i_xClearErr		:	BOOL;
	i_stTermAdsAddr	: 	ST_ELTerminal_AdsAddr;
END_VAR
VAR_OUTPUT
	q_xBusy			: 	BOOL;
	q_xError		: 	BOOL;
	q_udiErrId		:	UDINT;
END_VAR
VAR
	iState				: 	INT;
	rtExecute			: 	R_TRIG;
	ifbEcCoESdoWrite	: 	FB_EcCoESdoWrite;
	iDeletePowerCmd		: 	INT;
END_VAR
VAR CONSTANT
	DELETE_POWER_CMD: INT := 16#0004;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[CASE iState OF
	0: (* Idle State, waiting for a command *)
		rtExecute(CLK:=i_xExecute);
		IF rtExecute.Q THEN
			iState := iState + 10;
			q_xBusy := TRUE;
		END_IF

	10:  (* Parameterize FB to delete persistent data in the terminal *)
		iDeletePowerCmd := i_eChannel + DELETE_POWER_CMD;
	
		(* Format the AmsNetId from the terminal into a string *)
		ifbEcCoESdoWrite.sNetId	:= CONCAT(STR1:= BYTE_TO_STRING(i_stTermAdsAddr.netId[0]), STR2:= '.');
		ifbEcCoESdoWrite.sNetId	:= CONCAT(STR1:= ifbEcCoESdoWrite.sNetId, STR2:= BYTE_TO_STRING(i_stTermAdsAddr.netId[1]));
		ifbEcCoESdoWrite.sNetId	:= CONCAT(STR1:= ifbEcCoESdoWrite.sNetId, STR2:= '.');
		ifbEcCoESdoWrite.sNetId	:= CONCAT(STR1:= ifbEcCoESdoWrite.sNetId, STR2:= BYTE_TO_STRING(i_stTermAdsAddr.netId[2]));
		ifbEcCoESdoWrite.sNetId	:= CONCAT(STR1:= ifbEcCoESdoWrite.sNetId, STR2:= '.');
		ifbEcCoESdoWrite.sNetId	:= CONCAT(STR1:= ifbEcCoESdoWrite.sNetId, STR2:= BYTE_TO_STRING(i_stTermAdsAddr.netId[3]));
		ifbEcCoESdoWrite.sNetId	:= CONCAT(STR1:= ifbEcCoESdoWrite.sNetId, STR2:= '.');
		ifbEcCoESdoWrite.sNetId	:= CONCAT(STR1:= ifbEcCoESdoWrite.sNetId, STR2:= BYTE_TO_STRING(i_stTermAdsAddr.netId[4]));
		ifbEcCoESdoWrite.sNetId	:= CONCAT(STR1:= ifbEcCoESdoWrite.sNetId, STR2:= '.');
		ifbEcCoESdoWrite.sNetId	:= CONCAT(STR1:= ifbEcCoESdoWrite.sNetId, STR2:= BYTE_TO_STRING(i_stTermAdsAddr.netId[5]));

		ifbEcCoESdoWrite.nSlaveAddr	:=	i_stTermAdsAddr.port;
		ifbEcCoESdoWrite.nSubIndex	:= 	16#0001;
		ifbEcCoESdoWrite.nIndex		:= 	16#FB00;
		ifbEcCoESdoWrite.pSrcBuf	:= 	ADR(iDeletePowerCmd);
		ifbEcCoESdoWrite.cbBufLen	:= 	SIZEOF(iDeletePowerCmd);
		ifbEcCoESdoWrite.bExecute	:= 	TRUE;
		ifbEcCoESdoWrite.tTimeout	:=	T#5S;
		
		(* Check to see that FB is working *)
		IF ifbEcCoESdoWrite.bBusy THEN
			iState	:= 20;
		END_IF
		
		
	20:	(* Wait for FB to finish *)
		IF NOT ifbEcCoESdoWrite.bBusy THEN
			ifbEcCoESdoWrite.bExecute	:=FALSE;
			IF ifbEcCoESdoWrite.bError THEN
				q_udiErrId	:= ifbEcCoESdoWrite.nErrId;
				q_xError := TRUE;
				iState := 999;
			ELSE
				q_xBusy := FALSE;	
				iState := 0;
			END_IF
		END_IF


	999: (* Error State *)
		IF i_xClearErr THEN
			q_xError	:= FALSE;
			q_udiErrId	:= 0;
			q_xBusy		:= 0;
			iState		:= 0;
		END_IF

END_CASE

(* Write Value to 0xFB00:01 to save power values to F801:01:02:03 *)
ifbEcCoESdoWrite();

]]></ST>
    </Implementation>
    <ObjectProperties />
  </POU>
</TcPlcObject>