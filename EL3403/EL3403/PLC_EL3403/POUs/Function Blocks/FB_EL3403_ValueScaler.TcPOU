﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.0.24">
  <POU Name="FB_EL3403_ValueScaler" Id="{76664fe5-ec8f-4abf-83bb-596baad99c20}">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_EL3403_ValueScaler
(***************************************************************************
	This function block has been written for Terminal Firmware version 9
	
	Written by:	Paul Zurlinden
				Beckhoff Automation
	
	Date:		2014-07-17
***************************************************************************)
VAR_INPUT
	i_eTerminalType	: ENUM_EL3403_TYPE;
	i_stTermIns		: ST_EL3403_TerminalInputs;
	i_lrCtRatio		: LREAL;
END_VAR
VAR_IN_OUT
	iq_stTermOuts		: ST_EL3403_ChannelOutputs;
END_VAR
VAR_OUTPUT
	q_stActValue		: ST_EL3403_ChannelsScaled;
END_VAR
VAR
	lrActPwrMult_Ch1	: LREAL;
	lrActPwrMult_Ch2	: LREAL;
	lrActPwrMult_Ch3	: LREAL;

	lrCurrMult_Ch1		: LREAL;
	lrCurrMult_Ch2		: LREAL;
	lrCurrMult_Ch3		: LREAL;

	lrVoltMult_Ch1		: LREAL;
	lrVoltMult_Ch2		: LREAL;
	lrVoltMult_Ch3		: LREAL;
	
	lrAppPwrMult_Ch1	: LREAL;
	lrAppPwrMult_Ch2	: LREAL;
	lrAppPwrMult_Ch3	: LREAL;
	
	lrReactPwrMult_Ch1	: LREAL;
	lrReactPwrMult_Ch2	: LREAL;
	lrReactPwrMult_Ch3	: LREAL;
	
	lrEnergyMult_Ch1	: LREAL;
	lrEnergyMult_Ch2	: LREAL;
	lrEnergyMult_Ch3	: LREAL;
	
	lrcosPhiMult_Ch1	: LREAL;
	lrcosPhiMult_Ch2	: LREAL;
	lrcosPhiMult_Ch3	: LREAL;
	
	lrFreqMult_Ch1		: LREAL;
	lrFreqMult_Ch2		: LREAL;
	lrFreqMult_Ch3		: LREAL;
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[// Configure multipliers based on terminal type
CASE i_eTerminalType OF
	E_0000:
		lrActPwrMult_Ch1	:= lrActPwrMult_Ch2	:= lrActPwrMult_Ch3	:= 0.01;
		lrCurrMult_Ch1		:= lrCurrMult_Ch2	:= lrCurrMult_Ch3	:= 0.000001;
		lrVoltMult_Ch1		:= lrVoltMult_Ch2	:= lrVoltMult_Ch3	:= 0.0001;
		
		lrAppPwrMult_Ch1	:= lrAppPwrMult_Ch2		:= lrAppPwrMult_Ch3		:= 0.01;
		lrReactPwrMult_Ch1	:= lrReactPwrMult_Ch2	:= lrReactPwrMult_Ch3	:= 0.01;
		lrEnergyMult_Ch1	:= lrEnergyMult_Ch2		:= lrEnergyMult_Ch3		:= 0.001;
		lrcosPhiMult_Ch1	:= lrcosPhiMult_Ch2		:= lrcosPhiMult_Ch3		:= 0.001;
		lrFreqMult_Ch1		:= lrFreqMult_Ch2		:= lrFreqMult_Ch3		:= 0.1;
	

	E_0010:
		lrActPwrMult_Ch1	:= lrActPwrMult_Ch2	:= lrActPwrMult_Ch3	:= 0.01;
		lrCurrMult_Ch1		:= lrCurrMult_Ch2	:= lrCurrMult_Ch3	:= 0.000005;
		lrVoltMult_Ch1		:= lrVoltMult_Ch2	:= lrVoltMult_Ch3	:= 0.0001;
		
		lrAppPwrMult_Ch1	:= lrAppPwrMult_Ch2		:= lrAppPwrMult_Ch3		:= 0.01;
		lrReactPwrMult_Ch1	:= lrReactPwrMult_Ch2	:= lrReactPwrMult_Ch3	:= 0.01;
		lrEnergyMult_Ch1	:= lrEnergyMult_Ch2		:= lrEnergyMult_Ch3		:= 0.001;
		lrcosPhiMult_Ch1	:= lrcosPhiMult_Ch2		:= lrcosPhiMult_Ch3		:= 0.001;
		lrFreqMult_Ch1		:= lrFreqMult_Ch2		:= lrFreqMult_Ch3		:= 0.1;
	
	
	E_0100:
		lrActPwrMult_Ch1	:= lrActPwrMult_Ch2	:= lrActPwrMult_Ch3	:= 0.001;
		lrCurrMult_Ch1		:= lrCurrMult_Ch2	:= lrCurrMult_Ch3	:= 0.0000001;
		lrVoltMult_Ch1		:= lrVoltMult_Ch2	:= lrVoltMult_Ch3	:= 0.0001;
		
		lrAppPwrMult_Ch1	:= lrAppPwrMult_Ch2		:= lrAppPwrMult_Ch3		:= 0.001;
		lrReactPwrMult_Ch1	:= lrReactPwrMult_Ch2	:= lrReactPwrMult_Ch3	:= 0.001;
		lrEnergyMult_Ch1	:= lrEnergyMult_Ch2		:= lrEnergyMult_Ch3		:= 0.001;
		lrcosPhiMult_Ch1	:= lrcosPhiMult_Ch2		:= lrcosPhiMult_Ch3		:= 0.001;
		lrFreqMult_Ch1		:= lrFreqMult_Ch2		:= lrFreqMult_Ch3		:= 0.1;
	
	
	E_0111:
		lrActPwrMult_Ch1	:= 0.01;
		lrActPwrMult_Ch2	:= 0.001;
		lrActPwrMult_Ch3	:= 0.0001;
		
		lrCurrMult_Ch1		:= 0.000001;
		lrCurrMult_Ch2		:= 0.0000001;
		lrCurrMult_Ch3		:= 0.00000001;
		
		lrVoltMult_Ch1		:= lrVoltMult_Ch2	:= lrVoltMult_Ch3	:= 0.0001;
		
		lrAppPwrMult_Ch1	:= 0.01;
		lrAppPwrMult_Ch2	:= 0.001;
		lrAppPwrMult_Ch3	:= 0.0001;
		
		lrReactPwrMult_Ch1	:= 0.01;
		lrReactPwrMult_Ch2	:= 0.001;
		lrReactPwrMult_Ch3	:= 0.0001;
		
		lrEnergyMult_Ch1	:= 0.01;
		lrEnergyMult_Ch2	:= 0.001;
		lrEnergyMult_Ch3	:= 0.0001;
		
		lrcosPhiMult_Ch1	:= lrcosPhiMult_Ch2	:= lrcosPhiMult_Ch3	:= 0.001;
		lrFreqMult_Ch1		:= lrFreqMult_Ch2		:= lrFreqMult_Ch3	:= 0.1;

END_CASE


// Scale Values for Current, Power, and Voltage
// Channel 1
q_stActValue.stCh1.lrCurrent	:= (i_stTermIns.stCh1.diCurrent * lrCurrMult_Ch1) * i_lrCtRatio;
q_stActValue.stCh1.lrVoltage	:= (i_stTermIns.stCh1.diVoltage * lrVoltMult_Ch1) * SQRT(3);
q_stActValue.stCh1.lrActivePwr	:= (i_stTermIns.stCh1.diActivePwr * lrActPwrMult_Ch1) * i_lrCtRatio;
	
		
// Channel 2
q_stActValue.stCh2.lrCurrent	:= (i_stTermIns.stCh2.diCurrent * lrCurrMult_Ch2) * i_lrCtRatio;
q_stActValue.stCh2.lrVoltage	:= (i_stTermIns.stCh2.diVoltage * lrVoltMult_Ch2) * SQRT(3);
q_stActValue.stCh2.lrActivePwr	:= (i_stTermIns.stCh2.diActivePwr * lrActPwrMult_Ch2) * i_lrCtRatio;
			

// Channel 3	
q_stActValue.stCh3.lrCurrent	:= (i_stTermIns.stCh3.diCurrent * lrCurrMult_Ch3) * i_lrCtRatio;
q_stActValue.stCh3.lrVoltage	:= (i_stTermIns.stCh3.diVoltage * lrVoltMult_Ch3) * SQRT(3);
q_stActValue.stCh3.lrActivePwr	:= (i_stTermIns.stCh3.diActivePwr * lrActPwrMult_Ch3) * i_lrCtRatio;

	
// Scale Values, by Channel, for Variant Indexes
// Channel 1
CASE i_stTermIns.stCh1.usiIndex OF
	0: 	(* Apparent Power *);
		q_stActValue.stCh1.lrApparentPwr	:= (i_stTermIns.stCh1.diVariant * lrAppPwrMult_Ch1) * i_lrCtRatio;
		iq_stTermOuts.stCh1.usiIndex		:= i_stTermIns.stCh1.usiIndex + 1;
		
	1:	(* Reactive Power *);
		q_stActValue.stCh1.lrReactivePwr	:= (i_stTermIns.stCh1.diVariant * lrReactPwrMult_Ch1) * i_lrCtRatio;
		iq_stTermOuts.stCh1.usiIndex		:= i_stTermIns.stCh1.usiIndex + 1;
				
	2:	(* Energy *);
		q_stActValue.stCh1.lrEnergy		:= (i_stTermIns.stCh1.diVariant * lrEnergyMult_Ch1) * i_lrCtRatio;
		iq_stTermOuts.stCh1.usiIndex		:= i_stTermIns.stCh1.usiIndex + 1;
				
	3:	(* cosPhi *);
		q_stActValue.stCh1.lrcosPhi		:= (i_stTermIns.stCh1.diVariant * lrcosPhiMult_Ch1);
		iq_stTermOuts.stCh1.usiIndex		:= i_stTermIns.stCh1.usiIndex + 1;
			
	4:	(* Frequency *);
		q_stActValue.stCh1.lrFrequency		:= (i_stTermIns.stCh1.diVariant * lrFreqMult_Ch1);
		iq_stTermOuts.stCh1.usiIndex		:= i_stTermIns.stCh1.usiIndex + 1;

	5:	(* Negative Energy - Regen *);
		q_stActValue.stCh1.lrNegEnergy		:= (i_stTermIns.stCh1.diVariant * lrEnergyMult_Ch1) * i_lrCtRatio * -1;
		iq_stTermOuts.stCh1.usiIndex		:= i_stTermIns.stCh1.usiIndex + 0;				
			
END_CASE
		
// Channel 2
CASE i_stTermIns.stCh2.usiIndex OF
	0: 	(* Apparent Power *);
		q_stActValue.stCh2.lrApparentPwr	:= (i_stTermIns.stCh2.diVariant * lrAppPwrMult_Ch2) * i_lrCtRatio;
		iq_stTermOuts.stCh2.usiIndex		:= i_stTermIns.stCh2.usiIndex + 1;
		
	1:	(* Reactive Power *);
		q_stActValue.stCh2.lrReactivePwr	:= (i_stTermIns.stCh2.diVariant * lrReactPwrMult_Ch2) * i_lrCtRatio;
		iq_stTermOuts.stCh2.usiIndex		:= i_stTermIns.stCh2.usiIndex + 1;
			
	2:	(* Energy *);
		q_stActValue.stCh2.lrEnergy		:= (i_stTermIns.stCh2.diVariant * lrEnergyMult_Ch2) * i_lrCtRatio;
		iq_stTermOuts.stCh2.usiIndex		:= i_stTermIns.stCh2.usiIndex + 1;
				
	3:	(* cosPhi *);
		q_stActValue.stCh2.lrcosPhi		:= (i_stTermIns.stCh2.diVariant * lrcosPhiMult_Ch2);
		iq_stTermOuts.stCh2.usiIndex		:= i_stTermIns.stCh2.usiIndex + 1;
				
	4:	(* Frequency *);
		q_stActValue.stCh2.lrFrequency		:= (i_stTermIns.stCh2.diVariant * lrFreqMult_Ch2);
		iq_stTermOuts.stCh2.usiIndex		:= 0;

	5:	(* Negative Energy - Regen *);
		q_stActValue.stCh2.lrNegEnergy		:= (i_stTermIns.stCh2.diVariant * lrEnergyMult_Ch2) * i_lrCtRatio * -1;
		iq_stTermOuts.stCh2.usiIndex		:= i_stTermIns.stCh2.usiIndex + 0;				
			
END_CASE
	
// Channel 3
CASE i_stTermIns.stCh3.usiIndex OF
	0: 	(* Apparent Power *);
		q_stActValue.stCh3.lrApparentPwr	:= (i_stTermIns.stCh3.diVariant * lrAppPwrMult_Ch3) * i_lrCtRatio;
		iq_stTermOuts.stCh3.usiIndex		:= i_stTermIns.stCh3.usiIndex + 1;
					
	1:	(* Reactive Power *);
		q_stActValue.stCh3.lrReactivePwr	:= (i_stTermIns.stCh3.diVariant * lrReactPwrMult_Ch3) * i_lrCtRatio;
		iq_stTermOuts.stCh3.usiIndex		:= i_stTermIns.stCh3.usiIndex + 1;
				
	2:	(* Energy *);
		q_stActValue.stCh3.lrEnergy		:= (i_stTermIns.stCh3.diVariant * lrEnergyMult_Ch3) * i_lrCtRatio;
		iq_stTermOuts.stCh3.usiIndex		:= i_stTermIns.stCh3.usiIndex + 1;
				
	3:	(* cosPhi *);
		q_stActValue.stCh3.lrcosPhi		:= (i_stTermIns.stCh3.diVariant * lrcosPhiMult_Ch3);
		iq_stTermOuts.stCh3.usiIndex		:= i_stTermIns.stCh3.usiIndex + 1;
				
	4:	(* Frequency *);
		q_stActValue.stCh3.lrFrequency		:= (i_stTermIns.stCh3.diVariant * lrFreqMult_Ch3);
		iq_stTermOuts.stCh3.usiIndex		:= 0;

	5:	(* Negative Energy - Regen *);
		q_stActValue.stCh3.lrNegEnergy		:= (i_stTermIns.stCh3.diVariant * lrEnergyMult_Ch3) * i_lrCtRatio * -1;
		iq_stTermOuts.stCh3.usiIndex		:= i_stTermIns.stCh3.usiIndex + 0;				
			
END_CASE


// Calculate NET Energy from variants
q_stActValue.stCh1.lrNETEnergy	:= q_stActValue.stCh1.lrEnergy + q_stActValue.stCh1.lrNegEnergy;
q_stActValue.stCh2.lrNETEnergy	:= q_stActValue.stCh2.lrEnergy + q_stActValue.stCh2.lrNegEnergy;
q_stActValue.stCh3.lrNETEnergy	:= q_stActValue.stCh3.lrEnergy + q_stActValue.stCh3.lrNegEnergy;


// Scale values for energy retrieved from terminal persistent data
q_stActValue.stCh1.stStoredData.lrPosEnergy	:= iq_stTermOuts.stCh1.stPersistentData.udiStoredEnergy * lrEnergyMult_Ch1 * i_lrCtRatio;
q_stActValue.stCh1.stStoredData.lrNegEnergy	:= -1 * (iq_stTermOuts.stCh1.stPersistentData.udiNegStoredEnergy * lrEnergyMult_Ch1 * i_lrCtRatio);
q_stActValue.stCh1.stStoredData.lrNETEnergy	:= q_stActValue.stCh1.stStoredData.lrNegEnergy + q_stActValue.stCh1.stStoredData.lrPosEnergy;

q_stActValue.stCh2.stStoredData.lrPosEnergy	:= iq_stTermOuts.stCh2.stPersistentData.udiStoredEnergy * lrEnergyMult_Ch1 * i_lrCtRatio;
q_stActValue.stCh2.stStoredData.lrNegEnergy	:= -1 * (iq_stTermOuts.stCh2.stPersistentData.udiNegStoredEnergy * lrEnergyMult_Ch1 * i_lrCtRatio);
q_stActValue.stCh2.stStoredData.lrNETEnergy	:= q_stActValue.stCh2.stStoredData.lrNegEnergy + q_stActValue.stCh2.stStoredData.lrPosEnergy;

q_stActValue.stCh3.stStoredData.lrPosEnergy	:= iq_stTermOuts.stCh3.stPersistentData.udiStoredEnergy * lrEnergyMult_Ch1 * i_lrCtRatio;
q_stActValue.stCh3.stStoredData.lrNegEnergy	:= -1 * (iq_stTermOuts.stCh3.stPersistentData.udiNegStoredEnergy * lrEnergyMult_Ch1 * i_lrCtRatio);
q_stActValue.stCh3.stStoredData.lrNETEnergy	:= q_stActValue.stCh3.stStoredData.lrNegEnergy + q_stActValue.stCh3.stStoredData.lrPosEnergy;

]]></ST>
    </Implementation>
    <ObjectProperties />
  </POU>
</TcPlcObject>